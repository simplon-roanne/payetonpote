-- MySQL Script generated by MySQL Workbench
-- lun. 08 oct. 2018 16:05:05 CEST
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema payetonpote
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema payetonpote
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `payetonpote` DEFAULT CHARACTER SET utf8 ;
USE `payetonpote` ;

-- -----------------------------------------------------
-- Table `payetonpote`.`participant`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payetonpote`.`participant` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NULL,
  `email` VARCHAR(200) NULL,
  `campaign_id` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_participant_campaign1_idx` (`campaign_id` ASC),
  CONSTRAINT `fk_participant_campaign1`
    FOREIGN KEY (`campaign_id`)
    REFERENCES `payetonpote`.`campaign` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `payetonpote`.`campaign`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payetonpote`.`campaign` (
  `id` VARCHAR(32) NOT NULL,
  `title` VARCHAR(150) NULL,
  `content` TEXT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  `goal` INT NULL,
  `author_id` INT NULL,
  `name` VARCHAR(150) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_campaign_participant1_idx` (`author_id` ASC),
  CONSTRAINT `fk_campaign_participant1`
    FOREIGN KEY (`author_id`)
    REFERENCES `payetonpote`.`participant` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `payetonpote`.`spending`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payetonpote`.`spending` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `amount` INT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  `participant_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_spending_participant1_idx` (`participant_id` ASC),
  CONSTRAINT `fk_spending_participant1`
    FOREIGN KEY (`participant_id`)
    REFERENCES `payetonpote`.`participant` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `payetonpote`.`payment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payetonpote`.`payment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `amount` INT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  `participant_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_payment_participant1_idx` (`participant_id` ASC),
  CONSTRAINT `fk_payment_participant1`
    FOREIGN KEY (`participant_id`)
    REFERENCES `payetonpote`.`participant` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
